#!/usr/bin/env python3

from secret import flag
from Crypto.Util.number import *

p = getPrime(1024)
q = getPrime(1024)
n = p * q
phi = (p - 1) * (q - 1)
e = 0x10001  # 65537
d = inverse(e, phi)

with open("./ciphertexts", "w") as ciphertexts:
    for f in flag:
        ciphertexts.write(str(pow(f, e, n)) + "\n")
    ciphertexts.write(str(n))
