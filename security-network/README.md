<div align="center">

# CC2022-Network-Security
[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

Materiale didattico su basi di Network Security (CyberChallenge 2022 - Alma Mater Studiorum - Università di Bologna).
</div>

## Contenuti

- [Network_Security__Basi_e_cenni_teorici](https://gitlab.com/ulisse_lab/teaching-material/-/blob/master/security-network/Network_Security__Basi_e_cenni_teorici.pdf): lezione di teoria su basi di Computer Networks e Network Security (8/3/2022)

- [Network_Security__Laboratorio](https://gitlab.com/ulisse_lab/teaching-material/-/blob/master/security-network/Network_Security__Laboratorio.pdf): lezione di laboratorio su IDS, IPS e Suricata in dettaglio (11/3/2022).

- [Lab-challenge](https://gitlab.com/ulisse_lab/teaching-material/-/tree/master/security-network/Lab-challenge): challenge su file `.pcap` proposte in laboratorio, con soluzioni. Utilizo di Wireshark come tool di analisi del traffico.
