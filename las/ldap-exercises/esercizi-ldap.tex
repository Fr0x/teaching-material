\documentclass[aspectratio=169]{beamer}

\usetheme{ulisse}

\usepackage{tikz}
\usepackage{listings}

\usetikzlibrary{positioning}

\lstset {
	basicstyle=\footnotesize\sffamily\color{white},
	numbers=left,
	numberstyle=\tiny\sffamily,
	columns=fixed,
	showstringspaces=false,
	keepspaces,
	frame=tblr,
	backgroundcolor=\color{black},
	showtabs=false
}

\title{LDAP authentication}
\author{Laboratorio di Amministrazione di Sistemi T}
\date{18 Maggio 2021}

\begin{document}
	\startlayoutpage
	\maketitle
	\stoplayoutpage

	\begin{frame}[fragile]
		\frametitle{LDAP -- comandi base}
		\begin{itemize}
		\item slapd è il demone LDAP installato sulle macchine
			virtuali.
\begin{lstlisting}
systemctl (start|status|stop) slapd
\end{lstlisting}
		\item È possibile fare il dump dell'intera
			configurazione tramite il comando
			\texttt{slapcat}.  Utile per backup.
		\item La configurazione di LDAP utilizza un formato
			chiamato LDIF.
		\item È possibile effettuare ricerche usando il comando
			\texttt{ldapsearch}
\begin{lstlisting}
ldapsearch -Y EXTERNAL -H ldapi:/// -LLL -b dc=labammsis
\end{lstlisting}
			Questo comando può usare diverse forme di
			autenticazione:
			\begin{itemize}
				\item -Y EXTERNAL, per accedere con
					l'utente che invoca il comando (root).
				\item -x, per autenticazione
					\textbf{semplice} (password
					inviata in chiaro).
			\end{itemize}
			Inoltre è possibile specificare (switch -H) il
			server a cui connettersi, tramite diversi
			schemi:
			\begin{itemize}
				\item ldap:///, LDAP remoto (normalmente
				in chiaro!!!)
				\item ldapi:///, LDAP su socket locale
					alla macchina.
				\item ldaps:///, LDAP su TLS / SSL.
			\end{itemize}
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{LDAP -- LDIF}
		Per la creazione degli utenti è necessario
		\textit{mimare} come minimo gli attributi degli utenti
		UNIX.
		\begin{itemize}
			\item È necessario avere una lista di utenti con
				i loro attributi (p.e. la shell di login, vedi
				\texttt{/etc/passwd}).
			\item Ogni utente ha un gruppo relativo e può
				appartenere a gruppi ausiliari.
		\end{itemize}
		Per questo vengono creati nella gerarchia di LDAP
		diverse \textbf{Organizational Unit} (ou), di default
		\texttt{ou=People,dc=labammsis} per gli utenti e
		\texttt{ou=Groups,dc=labammsis} per i gruppi.

		\begin{center}
			\begin{tikzpicture}
				\node[draw] (dc)     {dc=labammsis};
				\node[draw, green, below=1em of dc] (admin)  {cn=admin};
				\node[draw, below left=1em and 1em of dc] (people) {ou=People};
				\node[draw, below right=1em and 2em of dc] (groups) {ou=Groups};
				\node[draw, green, below left=1em and 0em of people] (dave)   {uid=dave};
				\node[draw, green, below left=1em and 0em of groups] (daveg)  {cn=dave};
				\node[draw, green, below right=1em and 0em of people] (gio)    {uid=gio};
				\node[draw, green, below right=1em and 0em of groups] (giog)   {cn=gio};

				\draw (dc) -- (admin);
				\draw (dc) -- (people);
				\draw (dc) -- (groups);
				\draw (people) -- (dave);
				\draw (people) -- (gio);
				\draw (groups) -- (daveg);
				\draw (groups) -- (giog);
			\end{tikzpicture}
		\end{center}
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- Creazione OU}
		Come visibile tramite \texttt{slapcat}, le ou di default non sono disponibili appena
		installato il server.  Per questo motivo devono essere create tramite LDIF.
		\vspace{1em}
		Per effettuare questa operazione è necessario creare un file di testo con il seguente contenuto:
\begin{lstlisting}
dn: ou=People,dc=labammsis
objectclass: organizationalunit
ou: People
description: system's users
\end{lstlisting}
		È necessario fare login come amministratore usando lo schema ldap:///
\begin{lstlisting}
# ldapsearch -x -LLL -D cn=admin,dc=labammsis \
        -b dc=labammsis -w gennaio.marzo -H ldap:///
...
# ldapadd -x -D cn=admin,dc=labammsis -w gennaio.marzo \
        -H ldap:/// -f ./people.ldif 
adding new entry "ou=People,dc=labammsis"
\end{lstlisting}
	\end{frame}

	\begin{frame}
		\begin{alertblock}{Esercizio}
			Creare le OU \texttt{ou=People,dc=labammsis} e \texttt{ou=Groups,dc=labammsis}
			all'interno del server.  Controllare che siano
			state create con il comando \texttt{ldapsearch}
		\end{alertblock}
		\begin{center}
			\begin{tikzpicture}
				\node[draw] (dc)     {dc=labammsis};
				\node[draw, green, below=1em of dc] (admin)  {cn=admin};
				\node[draw, below left=1em and 1em of dc] (people) {ou=People};
				\node[draw, below right=1em and 2em of dc] (groups) {ou=Groups};

				\draw (dc) -- (admin);
				\draw (dc) -- (people);
				\draw (dc) -- (groups);
			\end{tikzpicture}
		\end{center}
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- Creazione Utenti e gruppi}
		Gli utenti e i gruppi vengono creati allo stesso modo,
		usando gli LDIF e le objectClass \texttt{posixAccount}
		e \texttt{posixGroup}.

		\begin{minipage}{.49\textwidth}
\begin{lstlisting}
dn: uid=dave,ou=People,dc=labammsis
objectClass: top
objectClass: posixAccount
objectClass: shadowAccount
objectClass: inetOrgPerson
givenName: ${GIVEN_NAME}
cn: Davide
sn: Berardi
mail: davide.berardi@unibo.it
uid: dave
uidNumber: 10000
gidNumber: 10000
homeDirectory: /home/dave
loginShell: /bin/bash
gecos: Davide Berardi
userPassword: {crypt}x
\end{lstlisting}
		\end{minipage}
		\begin{minipage}{.49\textwidth}
\begin{lstlisting}
dn: cn=dave,ou=Groups,dc=labammsis
objectClass: top
objectClass: posixGroup
gidNumber: 10000
\end{lstlisting}
		\end{minipage}
	\end{frame}

	\begin{frame}[fragile]
		\begin{alertblock}{Esercizio}
			Creare due utenti e gruppi con nome, uid e gid a
			piacere.  Devono avere come shell
			\texttt{/bin/bash}
		\end{alertblock}
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- Ricerca utenti e filtri}
		È possibile usare dei filtri per cercare all'interno di
		LDAP.  Questi possono essere usati con ldapsearch e
		sono simili al seguente:
\begin{lstlisting}
(
        &
                (objectClass=*)
                (uid=dave)
)
\end{lstlisting}
		Dove \texttt{\&} indica l'AND tra due condizioni, mentre 
		vengono selezionate tutte le objectClass e le entry che
		contengono lo uid \texttt{dave}.\\
		\vspace{1em}
		Un esempio di comando che effettua una ricerca su
		LDAP è il seguente:
\begin{lstlisting}
ldapsearch -x -LLL -D cn=admin,dc=labammsis -b dc=labammsis \
        -w gennaio.marzo -H ldap:///                        \
        '(&(loginShell=/bin/bash)(uidNumber>=10001)'
\end{lstlisting}
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- Configurazione Server}
		Gli utenti aggiunti su LDAP possono essere utilizzati
		per effettuare il login sul server.  Per effettuare
		questa operazione sono necessari due pacchetti non
		installati nella macchina:
\begin{lstlisting}
apt install libpam-ldap
apt install libnss-ldap
\end{lstlisting}
		Vi verranno chiesti in ordine:
		\begin{itemize}
			\item Il server LDAP da usare
			(\texttt{ldapi:///} per il
			server locale)
			\item La base dell'albero (per la vm
			\texttt{dc=labammsis})
			\item La versione di LDAP da usare (3)
			\item se l'admin di LDAP può essere considerato
			root della macchina locale (yes)
			\item se il DB richiede login (no, ma lo
			modificheremo in seguito)
			\item L'account amministratore (
				\texttt{cn=admin,dc=labammsis})
			\item La password di amministrazione
				(\texttt{gennaio.marzo})
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{LDAP Login -- Big picture}
		\begin{center}
		\begin{tikzpicture}
			\node[draw,minimum width=10em,minimum height=2em]
				(pamldap)
				{ldap};
			\node[draw,left=2em of pamldap,minimum width=10em,minimum height=2em]
				(pamfile)
				{file};
			\node[draw,right=2em of pamldap,minimum width=10em,minimum height=2em]
				(pamkerberos)
				{kerberos};
			\node[draw,below=2em of pamldap,minimum width=10em,minimum height=2em]
				(pam)
				{pam};
			\node[draw,below=2em of pam,minimum width=10em,minimum height=2em]
				(login)
				{login};
			\node[draw,left=2em of login,minimum width=10em,minimum height=2em]
				(ftp)
				{ftp};
			\node[draw,right=2em of login,minimum width=10em,minimum height=2em]
				(ssh)
				{ssh};

			\draw[->] (ssh)   -- (pam);
			\draw[->] (ftp)   -- (pam);
			\draw[->] (login) -- (pam);

			\draw[->] (pam) -- (pamkerberos);
			\draw[->] (pam) -- (pamldap);
			\draw[->] (pam) -- (pamfile);
		\end{tikzpicture}
		\end{center}
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- Configurazione LibNSS}
		Un ulteriore passaggio necessario è la configurazione
		del sistema per ricercare gli utenti (tramite il comando
		\texttt{getent passwd}) e i gruppi (tramite il comando
		\texttt{getent groups}) su LDAP.\\
		\vspace{1em}
		Questa configurazione è disponibile in
		\texttt{/etc/nsswitch.conf}, bisognerà inserire le
		seguenti direttive:
\begin{lstlisting}
passwd: files systemd ldap
group:  files systemd ldap
shadow:  files ldap
gshadow: files ldap
\end{lstlisting}
		A questo punto i seguenti comandi dovrebbero contenere
		gli utenti inseriti:
\begin{lstlisting}
# getent passwd | tail -3
Debian-snmp:x:116:120::/var/lib/snmp:/bin/false
dave:x:10000:10000:Davide:/home/dave:/bin/bash
gio:x:10001:10001:Andrea:/home/gio:/bin/bash
\end{lstlisting}
	\end{frame}

	\begin{frame}
	\begin{alertblock}{Esercizio}
		Configurare libnss per ottenere utenti e gruppi via
		LDAP.
	\end{alertblock}
		Comandi utili:
		\begin{itemize}
			\item ldapsearch
			\item getent passwd
			\item getent group
		\end{itemize}
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- Configurazione PAM}
		Per fare login via pam sarà necessario configurare le
		password dei vari utenti creati.  È disponibile il
		comando \texttt{ldappasswd}
\begin{lstlisting}
ldappasswd -D cn=admin,dc=labammsis -w gennaio.marzo \
        uid=dave,ou=People,dc=labammsis -s ciaociao
\end{lstlisting}
		È possibile specificare la password dell'utente in vari
		modi (p.e. da file o richiedendo la vecchia password).\\
		\vspace{1em}
		Inoltre è necessario configurare il file
		\texttt{/etc/ldap/ldap.conf} inserendo al suo interno le
		seguenti informazioni:
\begin{lstlisting}
BASE dc=labammsis
URI  ldapi:///
\end{lstlisting}
	\end{frame}

	\begin{frame}[fragile]
	\begin{alertblock}{Esercizio}
		Configurare pam per effettuare login via LDAP.
		Effettuare un login tramite il comando \texttt{login} e
		\texttt{ssh}.
	\end{alertblock}
		Controllate i warning tramite il file
		\texttt{/var/log/auth.log} !
\begin{lstlisting}
tail -f /var/log/auth.log
\end{lstlisting}
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- Modificare una entry}
		Per modificare una entry (p.e. la shell di un utente) è
		necessario usare il comando ldapmodify.\\
		\vspace{1em}
		Questo comando richiede di utilizzare un LDIF con al
		suo interno una direttiva \texttt{changeType} che indica
		il tipo di cambiamento tra \texttt{add}, \texttt{modify}
		e \texttt{delete}.
\begin{lstlisting}
root@las:~# cat chsh.ldif
dn: uid=dave,ou=People,dc=labammsis
changetype: modify
replace: loginShell
loginShell: /bin/sh
\end{lstlisting}
		Il comando segue la normale sintassi dei comandi LDAP.
		Ad esempio, il comando seguente, unito all'LDIF cambia
		la shell dell'utente dave.
\begin{lstlisting}
# ldapmodify -x -D cn=admin,dc=labammsis -w gennaio.marzo -f chsh.ldif
modifying entry "uid=dave,ou=People,dc=labammsis"
\end{lstlisting}
	\end{frame}

	\begin{frame}
		\begin{alertblock}{Esercizio}
			Cambiare la shell di uno dei due utenti creati a
			\texttt{/bin/nologin}.
		\end{alertblock}
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- Aggiungere utenti a un gruppo}
		Supponiamo di voler aggiungere gli utenti al gruppo
		\texttt{ammsis2021}.  Per fare questa operazione
		dovremo anzitutto creare il gruppo:
\begin{lstlisting}
# cat labammsis.ldif
dn: cn=ammsis2021,ou=Groups,dc=labammsis
objectClass: top
objectClass: posixGroup
gidNumber: 20000
# ldapadd -x -D cn=admin,dc=labammsis -w gennaio.marzo -H ldap:/// \
        -f labammsis.ldif
adding new entry "cn=ammsis2021,ou=Groups,dc=labammsis"
\end{lstlisting}
	\end{frame}
	\begin{frame}[fragile]
		\frametitle{LDAP -- Aggiungere utenti a un gruppo}
		Successivamente potremo aggiungere utenti al gruppo
		tramite il comando \texttt{ldapmodify}:
\begin{lstlisting}
# cat addlabammsis.ldif
dn: cn=ammsis2021,ou=Groups,dc=labammsis
changetype: modify
add: memberUid
memberUid: dave
# ldapmodify -x -D cn=admin,dc=labammsis -w gennaio.marzo \
        -f addlabammsis.ldif
modifying entry "cn=ammsis2021,ou=Groups,dc=labammsis"
\end{lstlisting}
	\end{frame}

	\begin{frame}
		\begin{alertblock}{Esercizio}
			Creare il gruppo ammsis2021 e aggiungere uno dei
			due utenti al gruppo ammsis2021.
		\end{alertblock}
	\end{frame}


	\begin{frame}
		\frametitle{LDAP -- Login remoto}
		È possibile configurare le macchine per effettuare il
		login tramite un server LDAP remoto.  Per effettuare
		questa operazione è sufficiente configurare i
		\texttt{client} per effettuare il login sul server
		remoto:
		\begin{itemize}
			\item Configurando libpam-ldap per effettuare le
				richieste all'ip del server (\texttt{URI
				ldap://10.2.2.2/}).
			\item Configurando libnss-ldap per effettuare le
				ricerche all'ip del server.
			\item Ricordatevi di configurare
				\texttt{/etc/ldap/ldap.conf} e
				\texttt{/etc/nsswitch.conf} !
		\end{itemize}
	\end{frame}

	\begin{frame}
		\begin{alertblock}{Esercizio}
			Configurare LDAP sulla macchina client per
			effettuare il login con gli utenti configurati
			sul server LDAP in esecuzione sulla macchina
			server.
		\end{alertblock}
		\begin{itemize}
			\item Ricordatevi il file /var/log/auth.log
			\item Configurate prima libnss per facilitare il
				debug.
		\end{itemize}
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- Sudo}
		Sudo può essere usato via LDAP installando il relativo
		pacchetto:
\begin{lstlisting}
# apt install sudo-ldap
\end{lstlisting}
		Per usare sudo dovremo caricare lo schema di sudo, è
		possibile vedere gli schemi caricati con
\begin{lstlisting}
# ldapsearch -H ldapi:/// -x -s base -b "cn=subschema" objectclasses
\end{lstlisting}
		È possibile notare come lo schema di sudo non sia
		presente, per caricarlo bisogna usare il seguente
		comando:
\begin{lstlisting}
ldapadd -Y EXTERNAL -H ldapi:/// \
        -f /usr/share/doc/sudo-ldap/schema.olcSudo
\end{lstlisting}
	\end{frame}
	\begin{frame}[fragile]
		\frametitle{LDAP -- Sudo}
		Successivamente sarà possibile creare la \texttt{ou}
		specifica per i \texttt{sudoers}:
\begin{lstlisting}
# cat sudoersou.ldif
dn: ou=SUDOers,dc=labammsis
objectclass: organizationalunit
ou: SUDOers
description: sudo enabled users
# ldapadd -x -D cn=admin,dc=labammsis -w gennaio.marzo \
        -H ldap:/// -f sudoersou.ldif
\end{lstlisting}
	\end{frame}
	\begin{frame}[fragile]
		\frametitle{LDAP -- Sudo}
	E la controparte del file \texttt{/etc/sudoers}, questo possiede
	gli attributi \texttt{sudoUser}, \texttt{sudoHost} e
	\texttt{sudoCommand} per limitare allo stesso modo le
	possibilità dell'utente.
\begin{lstlisting}
# cat sudoers.ldif
dn: cn=defaults,ou=SUDOers,dc=labammsis
objectClass: top
objectClass: sudoRole
cn: defaults
sudoUser: dave
sudoHost: ALL
sudoCommand: ALL
# ldapadd -x -D cn=admin,dc=labammsis -w gennaio.marzo \
        -H ldap:/// -f sudoers.ldif
\end{lstlisting}
	L'ultima configurazione necessaria è la modifica del file
	\texttt{/etc/ldap/ldap.conf} aggiungendo la variabile
	\texttt{SUDOERS\_BASE}:
\begin{lstlisting}
SUDOERS_BASE ou=SUDOers,dc=labammsis
\end{lstlisting}
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- disabilitazione anonymous bind}
		È possibile (per motivi di sicurezza) disabilitare i
		bind (ricerche e tentativi di login) non autenticati.\\
		\vspace{1em}
		In questo modo bisognerà creare degli account
		\textit{per le varie macchine richiedenti il login}.\\
		\vspace{1em}
		Questa configurazione non fa parte dell'albero LDAP
		\texttt{dc=labammsis} ma dell'albero di configurazione
		(accedibile tramite \texttt{-Y EXTERNAL} e \texttt{-H
		ldapi:///}).\\
		\vspace{1em}
		Questo albero di configurazione è
		dipendente dall'installazione di LDAP.  Nelle macchine
		virtuali del laboratorio utilizza una configurazione
		interna a LDAP chiamata olc (online configuration).
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- disabilitazione anonymous bind}
		La configurazione da utilizzare è la seguente:
\begin{lstlisting}
dn: cn=config
changetype: modify
add: olcDisallows
olcDisallows: bind_anon
-

dn: olcDatabase={-1}frontend,cn=config
changetype: modify
add: olcRequires
olcRequires: authc
\end{lstlisting}
		Si noti come il separatore aggiunge più comandi ad un
		singolo ldif utilizzabile tramite ldapmodify.\\
		\vspace{1em}
		Sarà quindi necessario usare il seguente comando per
		caricare la configurazione:
\begin{lstlisting}
# ldapmodify -Y EXTERNAL -H ldapi:/// -f disableanonbind.ldif
\end{lstlisting}
	\end{frame}


	\begin{frame}[fragile]
		\frametitle{LDAP -- Schemi custom}
		Un approccio utilizzato da molti software (p.e. gitlab,
		grafana, ...) è quello di controllare un parametro
		all'interno del record di un utente.\\
		\vspace{1em}
		Supponiamo di voler aggiungere il campo booleano
		\texttt{gitlabAdmin:} agli utenti per indicare se sono
		o meno amministratori del gitlab aziendale.\\
		\vspace{1em}
		Per effettuare questa operazione dobbiamo agire sugli
		\textbf{schemi} di LDAP.\\
		\vspace{1em}
		Sarà necessario creare una nuova objectClass in modo da
		\textit{associare} agli utenti il nuovo attributo e
		descrivere il tipo degli attributi associati a questa
		objectClass.
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- Schemi custom}
		Dovremo scegliere un OID di riferimento per la nostra
		objectClass, possiamo sceglierne uno libero (p.e. uno
		della gerarchia 1.7.1.1.1).
\begin{lstlisting}
dn: cn=gitlabextended,cn=schema,cn=config
objectClass: olcSchemaConfig
cn: gitlabextended
olcObjectClasses: (
        1.7.1.1.1.100                         # OID
        NAME 'gitlabextended'                 # Nome
        DESC 'extended user class for gitlab' # Descrizione
        SUP  top                              # Classe padre
        AUXILIARY # indica una classe non "base"
        MAY ( # Indica gli attributi opzionali
                gitlabAdmin
        )
)
\end{lstlisting}
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- Schemi custom}
		Oltre a questa configurazione è necessario specificare
		l'attributo da associare alla objectClass
\begin{lstlisting}
olcAttributeTypes: (
        1.7.1.1.1                    # OID
        NAME 'gitlabAdmin'           # Nome
        DESC 'gitlab administrator'  # Descrizione
        # OID che definisce il tipo Booleano
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
)
\end{lstlisting}
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- Schemi custom}
		Il file completo per la creazione di questo attributo è il
		seguente
\begin{lstlisting}
dn: cn=gitlabextended,cn=schema,cn=config
objectClass: olcSchemaConfig
cn: gitlabextended
olcObjectClasses: (
        1.7.1.1.1.100
        NAME 'gitlabextended'
        DESC 'extended user class for gitlab'
        SUP  top
        AUXILIARY
        MAY ( gitlabAdmin )
)
olcAttributeTypes: (
        1.7.1.1.1
        NAME 'gitlabAdmin'
        DESC 'gitlab administrator'
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
)
\end{lstlisting}
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- Schemi custom}
		Caricandolo usando il comando \texttt{ldapadd} e
		l'albero di configurazione (usando \texttt{-H
		ldapi:///} e \texttt{-Y EXTERNAL}) è possibile associare
		agli utenti l'attributo gitlabAdmin.
		\begin{lstlisting}
dn: uid=dave,ou=People,dc=labammsis
changeType: modify
add: objectClass
objectClass: gitlabextended

-

dn: uid=dave,ou=People,dc=labammsis
changeType: modify
add: gitlabAdmin
gitlabAdmin: FALSE
\end{lstlisting}
	\end{frame}

	\begin{frame}[fragile]
		\frametitle{LDAP -- Schemi custom}
		Usando il comando ldapmodify è possibile indicare che
		l'utente non è amministratore di gitlab.
\begin{lstlisting}
# ldapmodify -x -D cn=admin,dc=labammsis -w gennaio.marzo \
        -f addgitlabadmin.ldif
\end{lstlisting}
		\begin{alertblock}{Esercizio}
			Associare a uno dei due utenti l'attributo
			\texttt{gitlabAdmin: FALSE}
		\end{alertblock}
	\end{frame}

%	\begin{frame}
%		\frametitle{LDAP -- STARTTLS}
%	\end{frame}
	\startlayoutpage
	\begin{frame}
		\begin{center}
			\Huge Domande?
		\end{center}
	\end{frame}
	\stoplayoutpage
\end{document}
