#!/usr/bin/python3
from Crypto.Cipher import AES

from select import select

from flag import flag, key, padc


INTRO = """
Lol. You think you can steal my flag?
I\'ll even encrypt your input for you,
but you can\'t get my secrets!"""



def crypt(inp):
    cipher = AES.new(key, AES.MODE_ECB)

    plaintext = inp.strip() + flag

    l = len(plaintext)

    padl = (l // 32 + 1)*32 if l % 32 != 0 else 0

    plaintext = plaintext.ljust(padl, padc)

    ciphertext = (cipher.encrypt(plaintext)).hex()

    return ciphertext






if __name__ == '__main__':

    assert (len(flag) == 32) and (len(key) == 32)

    print(INTRO)

    while True:
        try:
            print('Enter your text here: ')

            inp = input()

            crypted = crypt(inp)

            print('Here\'s your encrypted text:\n{}\n\n'.format(crypted))
        except KeyboardInterrupt:
            exit(0)
