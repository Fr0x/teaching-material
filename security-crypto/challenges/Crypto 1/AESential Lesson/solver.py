from pwn import *


p = process("chal.py")

paddingChar = "C"
flag = ""
for i in range(32):
    for j in range(32,127):

        char = chr(j)
        text = char + flag + paddingChar * (31-i)
        send_text = text+"a"+"a"*i
        p.recvuntil("here:")
        p.sendline(send_text)

        #mangio -> Here's your encrypted text:
        p.recvline()

        #mangio -> \n
        p.recvline()

        #mangio il ciphertext
        cipher_text = p.recvline()

        #converto i bytes in stringa
        cipher_text = cipher_text.decode("utf-8")

        #rimuovo il newline finale
        cipher_text = cipher_text.strip()


        c1, c2, c3 = cipher_text[:64], cipher_text[64:128], cipher_text[128:192]


        if(c1 == c3):
            flag = char + flag

            break


for_the_meme(flag)
#chiudo processo
p.close()
