HTTP SMUGGLE
https://portswigger.net/web-security/request-smuggling/lab-basic-cl-te
https://portswigger.net/web-security/request-smuggling/lab-basic-te-cl

IDOR
https://portswigger.net/web-security/access-control/lab-insecure-direct-object-references

PATH-TRAVERSAL
https://portswigger.net/web-security/file-path-traversal/lab-simple
https://portswigger.net/web-security/file-path-traversal/lab-absolute-path-bypass
https://portswigger.net/web-security/file-path-traversal/lab-sequences-stripped-non-recursively
https://portswigger.net/web-security/file-path-traversal/lab-superfluous-url-decode

CSRF
https://portswigger.net/web-security/csrf/lab-no-defenses

SSRF
https://portswigger.net/web-security/ssrf/lab-basic-ssrf-against-localhost
https://portswigger.net/web-security/ssrf/lab-basic-ssrf-against-backend-system